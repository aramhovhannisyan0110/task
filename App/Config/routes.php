<?php

return [
    '' => [
        'method' => 'GET',
        'action' => 'Main@index'
    ],
    'login' => [
        'method' => 'GET',
        'action' => 'Main@login'
    ],
    'dashboard' => [
        'method' => 'GET',
        'action' => 'Admin@index'
    ],
];