<?php


namespace App\Core;


class View
{
    public $path;
    public $route;
    public $layout = 'default';

    public function __construct($route)
    {
        $this->route = $route;
    }

    public function render($title, $path, $vars = [])
    {
        extract($vars);
        ob_start();
        if (file_exists('App/View/'.$path.'.php') &&
            file_exists('App/View/Layout/' . $this->layout . '.php')) {
            require 'App/View/' . $path . '.php';
            $content = ob_get_clean();
            require 'App/View/Layout/' . $this->layout . '.php';
        } else {
            self::error(404);
        }
    }

    public static function redirect($url)
    {
        header('location:'.$url);
        exit();
    }

    public static function error($code)
    {
        http_response_code($code);
        require 'App/View/Error/'.$code.'.php';
        exit();
    }
}