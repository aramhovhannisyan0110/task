<?php


namespace App\Core;

use App\Core\View;
use App\Core\Middleware;

class Controller
{
    public $route;
    public $view;
    public $middleware;

    public function __construct($route)
    {
        $this->route = $route;
        $this->view = new View($this->route);
        $this->middleware = new Middleware();
    }
}