<?php


namespace App\Core;

use App\Core\View;

class Middleware
{
    public function auth()
    {
        if (key_exists('user', $_SESSION)) {
            return true;
        } else {
            View::redirect('/');
        }
    }
}