<?php


namespace App\Core;


class Router
{
    protected $routes = [];
    protected $params = [];

    public function __construct()
    {
        $arr = require 'App/Config/routes.php';
        foreach ($arr as $key => $value) {
            $this->add($key, $value);
        }
    }

    public function add($route, $params) {
        $route = '#^'.$route.'$#';
        $controller_action = explode('@', $params['action']);
        $this->routes[$route]['controller'] = $controller_action[0];
        $this->routes[$route]['action'] = $controller_action[1];
        $this->routes[$route]['method'] = $params['method'];
    }

    public function match() {
        $url = trim($_SERVER['REQUEST_URI'], '/');
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    public function run() {
        if($this->match()) {
            $method = $_SERVER['REQUEST_METHOD'];
            if ($method === $this->params['method']) {
                $path = 'App\Controller\\' . $this->params['controller'];
                if (class_exists($path)) {
                    $action = $this->params['action'];
                    if (method_exists($path, $action)) {
                        $controller = new $path($this->params);
                        $controller->$action();
                    } else {
                        View::error(404);
                    }
                } else {
                    View::error(404);
                }
            } else {
                View::error(404);
            }
        } else {
            View::error(404);
        }
    }
}