<?php


namespace App\Lib;

use PDO;

class Db
{
    protected $db;

    public function __construct()
    {
        $config = require 'App/Config/db.php';
        $this->db = new PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'].';',
            $config['user'], $config['password']);
    }

    private function query($sql)
    {
        return $this->db->query($sql);
    }

    public function select($table, $where = '')
    {
        $condition = $where ? ' WHERE '.$where : $where;
        $query = $this->query('SELECT * FROM '.$table.$condition);
        $res = $query->fetchAll();

        return $res;
    }
}