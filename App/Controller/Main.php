<?php


namespace App\Controller;


use App\Core\Controller;
use App\Lib\Db;

class Main extends Controller
{
    public function index()
    {
        $this->view->render('default', 'Login/index', ['name' => 'Task']);
    }

    public function login()
    {
        $this->view->render('default', 'Login/login');
    }
}